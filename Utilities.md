This document lists the tools we provide, e.g. regarding translations or handling media files.

**Contents**

[TOC]

## Asset optimization

See [Asset optimization](Asset-Optimization).

## Toolbar icon generation

Toolbar icon variants are generated from the individual icon components located
in skin/icons/toolbar/ and stored in that same directory.

### NPM commands

- `icons-gen`: Generate toolbar icon files.

## Translation linting

Translation files are checked for common problems based on a set of
[rules](#rules).

### Node scripts

- `build/locale-linter <file path>`: Check given translation files.

### Rules

#### File rules

- Locale name is valid.
- File has .json extension.
- File content is valid JSON.

#### String rules

- String ID is valid.
- String has "message" property.
- String message contains no newline characters.
- String message contains no leading/trailing spaces.
- String message contains no redundant spaces.

#### Word rules

- Important word contains no typos.

#### Placeholder rules

- Placeholder name is valid.
- String message contains all defined placeholders.
- String message contains no undefined placeholders.

#### Tag rules

- Tag is valid.
- Tag name is allowed.
- Tag content has no leading/trailing spaces.
- Slot tag is empty.

## Translation synchronization

### Crowdin

Community translations for the strings in this project are managed using
[Crowdin](https://crowdin.com/project/adblockplusui).

#### Prerequisites

- [Java 7+](https://support.crowdin.com/cli-tool/#requirements)
- [Crowdin CLI tool](https://support.crowdin.com/cli-tool/)

Follow [Crowdin's installation guide](https://support.crowdin.com/cli-tool/#installation)
in order to install its CLI tool.

#### Configuration

You can find our configuration for Crowdin CLI located in the `crowdin.yml` file
and more information in [Crowdin's documentation](https://support.crowdin.com/configuration-file/).

#### Environment variables

- `CROWDIN_PROJECT_ID`: Unique project ID - [retrievable from Crowdin project settings page](https://crowdin.com/project/adblockplusui/settings#api)(internal link).
- `CROWDIN_TOKEN`: API key string - [retrievable by translation managers](https://www.notion.so/Manual-testing-XTM-and-Crowdin-scripts-9bacb9a9be7e4afa800ca3bb937015c0#2bf0366373de4f219827bc94e2fe7b24)(internal link).

#### NPM commands

- `crowdin.download-translations`: Download translation updates from Crowdin.
- `crowdin.upload-strings`: Push source strings (en_US) to Crowdin.
- `crowdin.upload-translations`: Push translations to Crowdin.

### XTM

Translations for [core languages](Core-Languages-for-Translation) are
provided by a translation agency and are managed using
[XTM Cloud](https://xtm.cloud).

#### Environment variables

- `CLIENT`: Company ID string
- `PASSWORD`: User password string
- `USER_ID`: Numerical user ID

#### NPM commands

- `xtm.build`: Generate downloadadble files for `xtm.download`.
    Use only when translations need to be downloaded before XTM workflow finishes.
- `xtm.create`: Create a new project and upload source string changes.
- `xtm.download`: Download translations and apply changes to translation files.
- `xtm.update`: Upload and apply source string changes to project.

**Note:** The project name is expected to be name of the current git branch.

### CSV export

_Deprecated. Use [XTM utilities](#xtm) instead._

Translations for [core languages](Core-Languages-for-Translation) are
provided by a translation agency which is using CSV files.

#### NPM commands

- `csv-export -- <commit>`: Exports differences between source strings between
  current and given commit.
- `csv-import -- <file path>`: Imports translations from given file.

#### CSV file format

| Type     | Filename     | StringID | Description          | Placeholders                | en_US         | af         | am  | … |
|----------|--------------|----------|----------------------|-----------------------------|---------------|------------|-----|-----|
| Modified | options.json | cancel   | Cancel button label  |                             | Cancel        | Kanselleer | ይቅር | … |
| Added    | options.json | domain   | Domain input example | {"domain":{"content":"$1"}} | e.g. $domain$ |            |     | … |
