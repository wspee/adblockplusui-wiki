# ABP UI development workflow

- [Workflow](#workflow)
- [Branches](#branches)
- [Commits](#commits)

## Workflow

![](res/development-workflow/abp-ui-gitlab-feature-development-workflow.svg)

- New features are developed in issue branches that are squash-merged into
  **Feature** branches (see Feature #1, #2 and #3). **Feature** branches contain
  all changes that are required for the feature to be complete
  (incl. bug fixes, translations).
- Completed **Feature** branches are merged into the **Next** branch.
- Upon _**Feature Freeze**_, the **Next** branch is merged into the **Release**
  branch. Any bugs found during testing are merged directly into the **Release**
  branch.
- Upon _**Code Freeze**_, the **Release** branch is merged into the **Master**
  branch. Any hot fixes (e.g. "Bug fix #1") that are required before or after
  the release it made can be merged directly into the **Master** branch.
- If changes to strings were made, they are merged into a feature branch, listed
  in the issue under "Hints for translators" and uploaded to XTM to request
  translations. When those are ready, they are imported into the feature branch.
  See also [translation workflow](translation-workflow).

## Branches

- [Feature](#feature)
- [Next](#next)
- [Release](#release)
- [Master](#master)

### Feature

These branches are meant for features that cannot be released as-is, because
they consist of multiple issues and/or require other changes to be done first,
such as translations.

### [Next][gitlab-next]

This branch is the main development branch and contains all features that are
ready to be released. It represents the latest development version of Adblock
Plus.

### [Release][gitlab-release]

This branch represents the release candidate using which release testing is
done.

### [Master][gitlab-master]

This branch is being mirrored to [github.com/adblockplus/adblockplusui][github].
It represents the latest production version of Adblock Plus.

## Commits

- [Commit message](#commit-message)
- [Removal commits](#removal-commits)
- [Noissue commits](#noissue-commits)

### Commit message

We are following the [Conventional Commits][convcomms] specification
with the format `type: Message [#123]`:
- `type`: Any of the types in the [Angular convention][convcomms-types]
- `Message`: Commit message
- `123`: GitLab issue number, if any

Example: `build: Updated @eyeo/snippets dependency to 0.5.2 [#1214]`

### Removal commits

Dedicated removal commits should be made when removing modules, components or
other significant code parts that we may need to refer back to later on
(e.g. for reusing parts of a removed component).

Those commits should ideally be done as part of the same issue that makes the
removed code redundant. Alternatively, a follow-up issue can be created for the
purpose of removing such code.

### Noissue commits

Noissue commits are code changes that don't have an issue associated with them.
They lack context and further information for other stakeholders
(i.e. integrators, testers, translators) and are not part of the regular
development flow (i.e. no Copy LGTM, Legal LGTM, Feature QA, etc.).

Noissue commit messages should include a link to the respective review,
if applicable.

Allowed for…
- Adding release tags (no review required)
- Backing out changes (no review required)
- Importing translations for multiple issues from Crowdin and XTM (review
  required)

Debatable for…
- Updating minor versions of dependencies
- Backporting changes from other branch
- Fixing linting errors

Not allowed for…
- Minor bug fixes
- Changing metadata files (e.g. .gitignore)
- Changing build-related code
- Changing translation files
- Removing unused files/code
- …and everything that's not explicitly allowed


[convcomms]: https://www.conventionalcommits.org/en/v1.0.0/
[convcomms-types]: https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#type
[github]: https://github.com/adblockplus/adblockplusui
[gitlab-master]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/master
[gitlab-next]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/next
[gitlab-release]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/tree/release
